package se.experis.Notice.Board.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.Notice.Board.models.UserObject;

public interface UserObjectRepository extends JpaRepository<UserObject, Integer> {
    UserObject getById(Integer id);
}

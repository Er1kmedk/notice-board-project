package se.experis.Notice.Board.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.Notice.Board.models.Notice;

public interface NoticeRepository extends JpaRepository<Notice, Integer> {
    Notice getById(Integer id);
}

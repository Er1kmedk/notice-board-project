package se.experis.Notice.Board.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.Notice.Board.models.Notice;
import se.experis.Notice.Board.models.UserObject;
import se.experis.Notice.Board.repositories.NoticeRepository;
import se.experis.Notice.Board.repositories.UserObjectRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class NoticeBoardController {

    private ArrayList<UserObject> allUsers = new ArrayList<UserObject>();

    @Autowired
    NoticeRepository noticeRepository;
    @Autowired
    UserObjectRepository userObjectRepository;


    @GetMapping("/")
    public String listNotices(Model model){
        List<Notice> allNotices = noticeRepository.findAll();

        // CODE FOR SORTING THE LIST
//        Collections.sort(allNotices, new Comparator<Notice>() {
//            @Override
//            public int compare(Notice o1, Notice o2) {
//                return o1.getDate().compareTo(o2.getDate());
//            }
//        });

        model.addAttribute("allnotices", allNotices);
        model.addAttribute("credentialsObject", new UserObject());
        return "index";
    }


    @GetMapping("/login")
    public String login(@CookieValue(value = "userName", defaultValue = "") String userName, Model model) {
        if (userName.equals("")) {
            System.out.println(userName.equals(""));

            // Denna model är eventuellt överflödig
            model.addAttribute("usermodel", new UserObject());
            return "newuser";
        }
        else {
            List<Notice> allNotices = noticeRepository.findAll();
            model.addAttribute("allnotices", allNotices);
            return "loggedinuserview";
        }
    }

    @PostMapping("/createUser")
    public String createUser(@ModelAttribute() UserObject userObject, HttpServletResponse response) {

        // Creating cookie for the user
        Cookie userCookie = new Cookie("userName", userObject.getUserName());
        response.addCookie(userCookie);

        userObjectRepository.save(userObject);
        System.out.println("Account for " + userObject.getUserName() + " was successfully created!");

        return "redirect:/";
    }

    // Manipulate notices methods
    @GetMapping("/editnotice/{id}")
    public String editPost(@PathVariable Integer id, Model model) {
        if (noticeRepository.existsById(id)) {
            Optional<Notice> noticeToBeEdited = noticeRepository.findById(id);
            Notice notice = noticeToBeEdited.get();
            model.addAttribute("editNotice", notice);
        }
        return "editnotice";
    }

    @PostMapping("/editnotice/update/{id}")
    public String updateNotice(@PathVariable Integer id, @RequestBody Notice newNotice, Model model) {
        Optional<Notice> noticeRepo = noticeRepository.findById(id);
        Notice notice = noticeRepo.get();

        System.out.println("Updating notice: " + id);
        System.out.println("Old: " + notice.content);
        notice.content = newNotice.content;
        System.out.println("Content: " + newNotice.content);

        List<Notice> allNotices = noticeRepository.findAll();
        model.addAttribute("allnotices", allNotices);
        return "loggedinuserview";
    }

    @GetMapping("/deletenotice/{id}")
    public String deletePost(@PathVariable Integer id, Model model) {
        if (noticeRepository.existsById(id)) {
            noticeRepository.deleteById(id);
            System.out.println("Deleted notice " + id);
        }
        List<Notice> allNotices = noticeRepository.findAll();
        model.addAttribute("allnotices", allNotices);
        return "loggedinuserview";
    }
}

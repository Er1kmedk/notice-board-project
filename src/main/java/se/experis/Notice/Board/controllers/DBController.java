package se.experis.Notice.Board.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import se.experis.Notice.Board.models.CommonResponse;
import se.experis.Notice.Board.models.Notice;
import se.experis.Notice.Board.repositories.NoticeRepository;
import se.experis.Notice.Board.repositories.UserObjectRepository;
import se.experis.Notice.Board.utils.Command;
import se.experis.Notice.Board.utils.logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class DBController {

    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private UserObjectRepository userObjectRepository;

    @PostMapping("/notice")
    public Notice addNoticeBoard(HttpServletRequest request, HttpServletResponse response, @RequestBody Notice notice) {
        return noticeRepository.save(notice);
    }

    @GetMapping("/noticeboard/notice/all")
    public ResponseEntity<CommonResponse> getAllNotices(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = noticeRepository.findAll();
        cr.message = "All notices";

        HttpStatus resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/noticeboard/userobject/all")
    public ResponseEntity<CommonResponse> getAllUserObjects(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = userObjectRepository.findAll();
        cr.message = "All userobjects";

        HttpStatus resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

}

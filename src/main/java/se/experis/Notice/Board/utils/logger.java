package se.experis.Notice.Board.utils;

import java.util.ArrayList;

public class logger {

    private static logger loggerInstance = null;

    private ArrayList<Command> log;

    public String getLogString(){
        StringBuilder out =  new StringBuilder();

        for(Command cmd : log){
            out.append(cmd.toString());
            out.append(System.getProperty("line.separator"));
        }

        return out.toString();
    }

    public ArrayList<Command> getLog(){
        return log;
    }

    public void logCommand(Command cmd){
        System.out.println(cmd.toString());
        log.add(cmd);
    }

    private logger(){
        log = new ArrayList<Command>();
    }

    public static logger getInstance(){
        if (loggerInstance == null){
            loggerInstance = new logger();
        }

        return loggerInstance;
    }
}
